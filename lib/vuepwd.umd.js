(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["vuepwd"] = factory();
	else
		root["vuepwd"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "1f96":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_10_oneOf_1_0_node_modules_css_loader_index_js_ref_10_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_10_oneOf_1_2_node_modules_less_loader_4_1_0_less_loader_dist_cjs_js_ref_10_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_vue_pwd_vue_vue_type_style_index_0_id_0b708dcc_lang_less_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3161");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_10_oneOf_1_0_node_modules_css_loader_index_js_ref_10_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_10_oneOf_1_2_node_modules_less_loader_4_1_0_less_loader_dist_cjs_js_ref_10_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_vue_pwd_vue_vue_type_style_index_0_id_0b708dcc_lang_less_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_10_oneOf_1_0_node_modules_css_loader_index_js_ref_10_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_10_oneOf_1_2_node_modules_less_loader_4_1_0_less_loader_dist_cjs_js_ref_10_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_vue_pwd_vue_vue_type_style_index_0_id_0b708dcc_lang_less_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_10_oneOf_1_0_node_modules_css_loader_index_js_ref_10_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_10_oneOf_1_2_node_modules_less_loader_4_1_0_less_loader_dist_cjs_js_ref_10_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_vue_pwd_vue_vue_type_style_index_0_id_0b708dcc_lang_less_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "230e":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var document = __webpack_require__("7726").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "3161":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "6a99":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("d3f4");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "7726":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "79e5":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "7f7f":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc").f;
var FProto = Function.prototype;
var nameRE = /^\s*function ([^ (]*)/;
var NAME = 'name';

// 19.2.4.2 name
NAME in FProto || __webpack_require__("9e1e") && dP(FProto, NAME, {
  configurable: true,
  get: function () {
    try {
      return ('' + this).match(nameRE)[1];
    } catch (e) {
      return '';
    }
  }
});


/***/ }),

/***/ "86cc":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var toPrimitive = __webpack_require__("6a99");
var dP = Object.defineProperty;

exports.f = __webpack_require__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "9e1e":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("79e5")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "c69a":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
  return Object.defineProperty(__webpack_require__("230e")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "cb7c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "d3f4":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.function.name.js
var es6_function_name = __webpack_require__("7f7f");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"7a4e05bc-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/vue-pwd/vue-pwd.vue?vue&type=template&id=0b708dcc&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.dispay)?_c('div',{staticClass:"pay-password"},[_c('div',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.hide}),expression:"{methods: hide}"}],staticClass:"pwd-bg"}),_c('div',{staticClass:"pwd-box"},[_c('h1',[_vm._v("输入支付密码")]),_c('ul',{staticClass:"pwd-list"},[_c('li',[_vm._v(_vm._s(_vm.password.length >= 1 ? '●' : ''))]),_c('li',[_vm._v(_vm._s(_vm.password.length >= 2 ? '●' : ''))]),_c('li',[_vm._v(_vm._s(_vm.password.length >= 3 ? '●' : ''))]),_c('li',[_vm._v(_vm._s(_vm.password.length >= 4 ? '●' : ''))]),_c('li',[_vm._v(_vm._s(_vm.password.length >= 5 ? '●' : ''))]),_c('li',[_vm._v(_vm._s(_vm.password.length >= 6 ? '●' : ''))])]),_c('div',{staticClass:"pwd-link"},[_c('a',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.goto}),expression:"{methods: goto}"}]},[_vm._v("忘记支付密码？")])]),_c('ul',{staticClass:"pwd-keys"},[_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '1'}),expression:"{methods: keysOn, key: '1'}"}]},[_vm._v("1")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '2'}),expression:"{methods: keysOn, key: '2'}"}]},[_vm._v("2")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '3'}),expression:"{methods: keysOn, key: '3'}"}]},[_vm._v("3")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '4'}),expression:"{methods: keysOn, key: '4'}"}]},[_vm._v("4")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '5'}),expression:"{methods: keysOn, key: '5'}"}]},[_vm._v("5")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '6'}),expression:"{methods: keysOn, key: '6'}"}]},[_vm._v("6")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '7'}),expression:"{methods: keysOn, key: '7'}"}]},[_vm._v("7")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '8'}),expression:"{methods: keysOn, key: '8'}"}]},[_vm._v("8")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '9'}),expression:"{methods: keysOn, key: '9'}"}]},[_vm._v("9")]),_c('li',{staticClass:"no-bg"}),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: '0'}),expression:"{methods: keysOn, key: '0'}"}]},[_vm._v("0")]),_c('li',{directives:[{name:"tap",rawName:"v-tap",value:({methods: _vm.keysOn, key: 'del'}),expression:"{methods: keysOn, key: 'del'}"}],staticClass:"no-bg"},[_c('i',{staticClass:"iconfont slj-iconfontcha"})])])])]):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./packages/vue-pwd/vue-pwd.vue?vue&type=template&id=0b708dcc&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/vue-pwd/vue-pwd.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var vue_pwdvue_type_script_lang_js_ = ({
  name: 'vue-pwd',
  props: ['isDisplay'],
  data: function data() {
    return {
      password: [],
      // 存储密码的数组
      dispay: this.isDisplay // 是否显示密码框

    };
  },
  created: function created() {},
  methods: {
    // 虚拟键盘按键事件
    keysOn: function keysOn(d) {
      if (d.key != 'del') {
        if (this.password.length < 6 && d.key) {
          this.password.push(d.key);

          if (this.password.length == 6) {
            this.$emit('getData', {
              pwd: this.password
            }); // 当位数满6个时，触发父组件定义的事件，父组件定义事件的方法（v-on:getData="methods方法"）
          }
        }
      } else {
        this.password.pop();
      }
    },
    // 密码输入完成后，供父组件调用的方法（当密码正确时，关闭密码框，错误时弹出提示，并可重新输入）
    // 此方法在父组件调用方法为 this.$refs.payPassword.tips(false); 参数为布尔值。
    tips: function tips(bln) {
      if (bln) {
        this.dispay = false;
        this.password = [];
      } else {
        this.dispay = true;
        this.password = [];

        if (!alert('密码错误，请重新输入！')) {
          this.dispay = false;
        }
      }
    },
    // 手动打开关闭密码框
    open: function open(bln) {
      if (bln) this.dispay = true;else this.dispay = false;
    },
    hide: function hide() {
      this.dispay = false;
    },
    goto: function goto() {
      this.$emit('link', {
        pwd: this.password
      });
    }
  }
});
// CONCATENATED MODULE: ./packages/vue-pwd/vue-pwd.vue?vue&type=script&lang=js&
 /* harmony default export */ var vue_pwd_vue_pwdvue_type_script_lang_js_ = (vue_pwdvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./packages/vue-pwd/vue-pwd.vue?vue&type=style&index=0&id=0b708dcc&lang=less&scoped=true&
var vue_pwdvue_type_style_index_0_id_0b708dcc_lang_less_scoped_true_ = __webpack_require__("1f96");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./packages/vue-pwd/vue-pwd.vue






/* normalize component */

var component = normalizeComponent(
  vue_pwd_vue_pwdvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "0b708dcc",
  null
  
)

component.options.__file = "vue-pwd.vue"
/* harmony default export */ var vue_pwd = (component.exports);
// CONCATENATED MODULE: ./packages/vue-pwd/index.js

 // 导入组件，组件必须声明 name

 // 为组件提供 install 安装方法，供按需引入

vue_pwd.install = function (Vue) {
  Vue.component(vue_pwd.name, vue_pwd);
}; // 默认导出组件


/* harmony default export */ var packages_vue_pwd = (vue_pwd);
// CONCATENATED MODULE: ./packages/index.js

 // 导入颜色选择器组件

 // 存储组件列表

var components = [packages_vue_pwd]; // 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册

var install = function install(Vue) {
  // 判断是否安装
  if (install.installed) return; // 遍历注册全局组件

  components.map(function (component) {
    return Vue.component(component.name, component);
  });
}; // 判断是否是直接引入文件


if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

/* harmony default export */ var packages_0 = ({
  // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
  install: install,
  // 以下是具体的组件列表
  vuePwd: packages_vue_pwd
});
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (packages_0);



/***/ })

/******/ });
});
//# sourceMappingURL=vuepwd.umd.js.map