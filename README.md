# vue-pwd

## 安装（需要安装v-tap依赖）
```
npm i vue-pwd -S
npm i v-tap -S
```

### 导入
```
import Vue from 'vue';
import vuePwd from 'vue-pwd';
import 'vue-pwd/lib/vuepwd.css';

Vue.use(vuePwd);
```
