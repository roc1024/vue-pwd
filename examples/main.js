import Vue from 'vue'
import vueTap from 'v-tap'
import App from './App.vue'

// 导入组件库
import vuePwd from './../packages/index'
// 注册组件库
Vue.use(vueTap)
Vue.use(vuePwd)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
